#include QMK_KEYBOARD_H

#define _______ KC_TRNS
#define NKTG MAGIC_TOGGLE_NKRO
/* LAYOUT_60_ansi_split_bs
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
 * │00 │01 │02 │03 │04 │05 │06 │07 │08 │09 │0a │0b │0c │0d │0e │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┤
 * │10   │12 │13 │14 │15 │16 │17 │18 │19 │1a │1b │1c │1d │1e   │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤
 * │20    │22 │23 │24 │25 │26 │27 │28 │29 │2a │2b │2c │2d      │
 * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────┬───┤
 * │30      │32 │33 │34 │35 │36 │37 │38 │39 │3a │3b │3d        │
 * ├────┬───┴┬──┴─┬─┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬┴───┤
 * │40  │41  │43  │46                      │4a  │4b  │4d  │4e  │
 * └────┴────┴────┴────────────────────────┴────┴────┴────┴────┘
*/
#define LAYOUT_60_ansi_split_bs( \
    k00, k01, k02, k03, k04, k05, k06, k07, k08, k09, k0a, k0b, k0c, k0d, k0e, \
    k10,      k12, k13, k14, k15, k16, k17, k18, k19, k1a, k1b, k1c, k1d, k1e, \
    k20,      k22, k23, k24, k25, k26, k27, k28, k29, k2a, k2b, k2c, k2d,      \
    k30,      k32, k33, k34, k35, k36, k37, k38, k39, k3a, k3b,      k3d,      \
    k40, k41,      k43,           k46,                k4a, k4b,      k4d, k4e  \
) { \
    { k00,   k01,   k02,   k03,  k04,   k05,   k06,  k07,   k08,   k09,   k0a,   k0b,  k0c,   k0d,  k0e   }, \
    { k10,   KC_NO, k12,   k13,  k14,   k15,   k16,  k17,   k18,   k19,   k1a,   k1b,  k1c,   k1d,  k1e   }, \
    { k20,   KC_NO, k22,   k23,  k24,   k25,   k26,  k27,   k28,   k29,   k2a,   k2b,  k2c,   k2d,  KC_NO }, \
    { k30,  KC_NO, k32,   k33,  k34,   k35,   k36,  k37,   k38,   k39,   k3a,  k3b,  KC_NO, k3d,   KC_NO },  \
    { k40,  k41,   KC_NO, k43,  KC_NO, KC_NO, k46,  KC_NO, KC_NO, KC_NO, k4a,  k4b,  KC_NO, k4d,   k4e   }  \
}


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

	LAYOUT_60_ansi_split_bs(
            KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_DEL, KC_BSPC,
            KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS,
            KC_CAPS, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT,
            KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT,
            KC_LCTL, KC_LALT, KC_LGUI,           KC_SPC,         KC_RGUI, KC_RALT, KC_RCTL, MO(1)),

	LAYOUT_60_ansi_split_bs(
            KC_GRV, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, RGB_HUD, RGB_HUI,
            _______, _______, KC_UP, _______, _______, _______, _______, _______, _______, KC_INS, KC_PSCR, KC_SLCK, KC_PAUS, RGB_MOD,
            _______, KC_LEFT, KC_DOWN, KC_RGHT, _______, _______, _______, _______, _______, _______, KC_HOME, KC_PGUP, _______,
            _______, _______, _______, _______, _______, _______, NKTG, _______, KC_HOME, KC_END, KC_PGDN, _______,
            _______, _______, _______, _______, _______, _______, _______, _______),
};

void keyboard_post_init_user(void) {
    // Call the post init code.
    rgblight_enable_noeeprom(); // enables Rgb, without saving settings
	if (IS_LED_ON(usb_led, USB_LED_CAPS_LOCK)) {
        rgblight_mode_noeeprom(RGBLIGHT_MODE_STATIC_LIGHT);  // rgblight_sethsv_noeeprom(130,130,255);
        rgblight_sethsv_noeeprom(140,130,255);
    } else {
		rgblight_mode_noeeprom(RGBLIGHT_MODE_RAINBOW_SWIRL + 3);
        rgblight_sethsv_noeeprom(140,255,255);}
}
