#include QMK_KEYBOARD_H

#define _______ KC_TRNS
#define NKTG MAGIC_TOGGLE_NKRO

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

	LAYOUT_60_ansi_split_bs_rshift(
            KC_GRV, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSPC, KC_DEL,
            KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS,
            KC_CAPS, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT,
            KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT, KC_ESC,
            KC_LCTL, KC_LGUI, KC_LALT,            KC_SPC,         KC_RALT, MO(1), KC_APP, KC_RCTL),

	LAYOUT_60_ansi_split_bs_rshift(
            _______, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, _______, _______,
            _______, _______, KC_UP, _______, _______, _______, _______, _______, _______, KC_INS, KC_PSCR, KC_SLCK, KC_PAUS, _______,
            _______, KC_LEFT, KC_DOWN, KC_RGHT, _______, _______, _______, _______, _______, _______, KC_HOME, KC_PGUP, _______,
            _______, _______, _______, _______, _______, _______, NKTG, _______, KC_HOME, KC_END, KC_PGDN, _______, _______,
            _______, _______, _______, _______, _______, _______, _______, _______),
};

void keyboard_post_init_user(void) {
    // Call the post init code.
    rgblight_enable_noeeprom(); // enables Rgb, without saving settings
    rgblight_mode_noeeprom(RGBLIGHT_MODE_BREATHING + 2); // sets mode to Slow breathing without saving
    //rgblight_sethsv_noeeprom(140,130,255);
}

/*
void rgb_matrix_indicators_user(void)
{
    if (IS_HOST_LED_ON(USB_LED_CAPS_LOCK)) {
        //rgb_matrix_set_color(8, 0xFF, 0xFF, 0xFF);
        rgblight_sethsv_noeeprom(200,130,255);
    }
    else
    {
        rgblight_sethsv_noeeprom(140,130,255);
    }
} */


void led_set_user(uint8_t usb_led) {
	if (IS_LED_ON(usb_led, USB_LED_CAPS_LOCK)) {
		rgblight_sethsv_noeeprom(8,140,255);
	} else {
		rgblight_sethsv_noeeprom(140,255,255);
	}
}
